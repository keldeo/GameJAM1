from cocos.layer import Layer
from cocos.sprite import Sprite

class Game_over(Layer):
    def __init__(self,image):
        super(Game_over,self).__init__()
        self.affichage=Sprite(image)
        self.affichage.scale=2
        self.affichage.x=self.affichage.width/2+55
        self.affichage.y=self.affichage.height/2
        self.add(self.affichage)
